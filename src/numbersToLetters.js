/**
 * getupinterview
 *
 * Copyright © 2016 jeznag. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import _ from 'lodash';

export default {

  numberToLetterMapping: {
    0: [],
    1: [],
    2: ['A', 'B', 'C'],
    3: ['D', 'E', 'F'],
    4: ['G', 'H', 'I'],
    5: ['J', 'K', 'L'],
    6: ['M', 'N', 'O'],
    7: ['P', 'Q', 'R', 'S'],
    8: ['T', 'U', 'V'],
    9: ['W', 'X', 'Y', 'Z'],
  },

  telephoneWords(numbers) {
    const possibleLetters = numbers.map(number => this.numberToLetterMapping[number]);
    const permutationsOfLetters = possibleLetters[0].map(firstLetter => {
      const otherPossibleLetters = possibleLetters.slice(1);
      if (!otherPossibleLetters.length) {
        return firstLetter;
      }
      return otherPossibleLetters.map(otherLetterCombos =>
        otherLetterCombos.map(otherLetter => {
          if (otherLetter !== firstLetter) {
            return `${firstLetter}${otherLetter}`;
          }
          return null;
        }
      )).filter(combo => !!combo);
    });

    const uniquePermutations = _.uniq(_.flattenDeep(permutationsOfLetters));
    return uniquePermutations.join(', ');
  },
};
