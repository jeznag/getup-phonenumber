/**
 * getupinterview
 *
 * Copyright © 2016 jeznag. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import { expect } from 'chai';
import NumbersToLetters from '../src/numbersToLetters';

describe('NumbersToLetters converter', () => {

  const testCases = [
    {
      description: 'should correctly handle a single number',
      input: [2],
      expectedOutput: 'A, B, C',
    },
    {
      description: 'should correctly handle two different numbers',
      input: [2, 5],
      expectedOutput: 'AJ, AK, AL, BJ, BK, BL, CJ, CK, CL',
    },
    {
      description: 'should ignore 0 and 1',
      input: [0, 1],
      expectedOutput: '',
    },
  ];

  testCases.forEach(testCase => {
    it(`${testCase.input} should return ${testCase.expectedOutput}`, () => {
      expect(NumbersToLetters.telephoneWords(testCase.input)).to.equal(testCase.expectedOutput);
    });
  });

});
